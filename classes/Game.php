<?php

class Game{

	private $players;
	private $currentTradeWinner;
	private $currentTradeResults;

	/**
	 *  Constructeur
	 */
	
	public function __construct(){		

	}

	/**
	 * Accesseurs
	 */
	 	
	public function getPlayers(){
		return $this->players;
	}

	public function getWinner(){
		return $this->winner;
	}

	public function getCurrentTradeResults(){
		return $this->currentTradeResults;
	}

	public function getCurrentTradeWinner(){
		return $this->currentTradeWinner;
	}


	/**
	 *  Init
	 */
	
	public function init($_players){

		$this->players = $_players;
		// Creation d'une nouvelle pile de carte
		$this->createPilesOfCards();

	}

	/**
	 * Creation de la pile de carte
	 */

	private function createPilesOfCards(){

		//Construction du jeu de 52 Cartes
		$pileOfCards = range(1, 52);

		//Mélange des cartes
 		shuffle($pileOfCards);

		//Distribution des cartes aux deux joueurs
		//Comme dans une vraie distribution, on donne une carte sur deux
		foreach ($pileOfCards as $key => $value) {
			if ($key % 2 == 0) {
				$this->players[0]->addCartToHand($value);
			}
			else {
				$this->players[1]->addCartToHand($value);
			}
		}
	}

	/**
	 * Gestion d'une manche
	 */

	public function trade()
	{
		//On dépile la premiere carte de chaque main
		$results = [$this->players[0]->unpileCart(),$this->players[1]->unpileCart()];
		
		if( $results[0] > $results[1]){
			//player 1 gagne cette manche
			$this->currentTradeWinner = $this->players[0];
		}else{
			//player 2 gagne cette manche
			$this->currentTradeWinner = $this->players[1];		
		}		

		$this->currentTradeResults = $results;
		$this->currentTradeWinner->addOneToScore();

		if(count($this->currentTradeWinner->getHand()) > 0){

			/**
			 * Amelioration : Tester Si la difference des deux scores est plus élevée
			 * que le nombre de trades restants : 
			 * le vainqueur est celui dont le score est le plus élevé
			 */
			
			return true;
		}else{
			
			// Fin du tas de carte			
			if($this->players[0]->getScore() > $this->players[1]->getScore()){
				$this->winner = $this->players[0];
			}else{
				$this->winner = $this->players[1];
			}
			return false;
		}


		
	}
}

