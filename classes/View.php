<?php



class View{

	private $steps;

	/**
	 *  Constructeur
	 */
	
	public function __construct(){		

		system('clear');	
 		
	}

	public static function startGame(){
		print"    
		      _             _                                 _     
		_ __ | | __ _ _   _(_)_ __   __ _    ___ __ _ _ __ __| |___ 
		| '_ \| |/ _` | | | | | '_ \ / _` |  / __/ _` | '__/ _` / __|
		| |_) | | (_| | |_| | | | | | (_| | | (_| (_| | | | (_| \__ \
		| .__/|_|\__,_|\__, |_|_| |_|\__, |  \___\__,_|_|  \__,_|___/
		|_|            |___/         |___/                           
		\n\n";
	}

	/*
	Affichage d'une question, 
	Fonction recursive tant que la réponse est vide.
	*/
	public static function displayQuestion($_question){

		print $_question;
		$handle = fopen ("php://stdin","r");
		$line = trim(fgets($handle));
		if($line){
			return $line;
		}
		print "Mauvaise réponse, on recommence \n";
		View::displayQuestion($_question);					
	}

	/**
	 * Affichage d'un message simple avec CTA
	 */
	public static function displayMessage($_message){
		print $_message;
		$handle = fopen ("php://stdin","r");
		$line = trim(fgets($handle));
	}

	/**
	 * Affichage d'une erreur et fermeture
	 */
	public static function displayError($_message){
		system('clear');
		print($_message."\n\n");
		exit;
	    fclose($handle);
	}

	/**
	 * Affichage d'une manche
	 */
	public static function displayTrade($_game, $_inc){

		system('clear');
		//On récupère les joueurs de la game pour plus de lisibilité
		$players = $_game->getPlayers();
		//On Récupère les scores de la manche en cours
		$tradeResults = $_game->getCurrentTradeResults();
		// Afichage des données
		print "\n\n### Manche N° ".$_inc." ###";
		printf("\n%s gagne la manche avec %s contre %s",$_game->getCurrentTradeWinner()->getName(),max($tradeResults),min($tradeResults));
		printf("\n\nScores : %s - %s // %s - %s ",$players[0]->getName(), $players[0]->getScore(), $players[1]->getName(), $players[1]->getScore()); 
		print "\n(Appuyez sur une touche pour la manche suivante)";
		//CTA
		$handle = fopen ("php://stdin","r");
		$line = trim(fgets($handle));
	}

	/**
	 * Affichage de la fin de partie
	 */
	public static function displayWinner($game){
		system('clear');
		//On récupère les joueurs de la game pour plus de lisibilité
		$players = $game->getPLayers();
		//Affichage des données
		print "\n###########################\n";
		print strtoupper($game->getWinner()->getName())." WIN !!!";
		printf("\n\nScore Final : %s - %s // %s - %s ",$players[0]->getName(), $players[0]->getScore(), $players[1]->getName(), $players[1]->getScore()); 
		print "\n###########################\n";

		/**
		 * @Amelioration :
		 * On pourrait penser à proposer de relancer la partie, en reinitialisation les scores et la distribution des cartes
		 */
	}
}

