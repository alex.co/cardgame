<?php

class Player{

	private $name;
	private $hand;	
	private $score = 0;

	/**
	 *  Constructeur
	 */
	
	public function __construct($_name){

		$this->name = $_name;
	}

	/**
	 * Accesseurs
 	 */

	public function getHand(){
		return $this->hand;
	}
	public function getName(){
		return $this->name;		
	}
	public function getScore(){
		return $this->score;		
	}

	//Ajout d'un point au score
	public function addOneToScore(){
		++$this->score;
	}

	//Ajout d'une carte à la main du joueur
	public function addCartToHand($_value){
		$this->hand[] = $_value;
	}

	/**
	 * Le joueur retourne une carte, on retourne une variable et on enlève une carte de la main
	 * On retourne False s'il n'y a plus de carte.
	 */
	public function unpileCart(){
		if($val = array_shift($this->hand)){
			return $val;
		}
		return false;
	}
}