<?php

spl_autoload_register('autoloader');

function autoloader($classname) {
    include_once 'classes/' . $classname . '.php';
}

?>