<?php

//REQUIRE
 require ('core/init.php');


//les règles :

/*
* 52 cartes, on simplifie en utilisant simplement des valeurs de 1 à 52
* les cartes sont mélangées et distribuées à 2 joueurs
* chaque joueur retourne la première carte de son paquet, le joueur disposant de la plus forte carte marque un point
* on continue jusqu'à ce qu'il n'y ait plus de carte à jouer
* on affiche le nom du vainqueur
*/

View::startGame();
$game = new Game(); 

//1 - Init Game
if('oui' != View::displayQuestion("Et si on jouait à la bataille ? Tape 'oui' pour commencer : ")):
	View::displayError("Dommage. Bye.");
endif;

//2 - Get informations
$playerOneName = View::displayQuestion("Quel est le nom du premier joueur ? :");
$playerOne = new Player($playerOneName);

$playerTwoName = View::displayQuestion("Quel est le nom du second joueur ? :");
$playerTwo = new Player($playerTwoName);

//3- Start Game 
View::displayMessage(sprintf("C'est parti %s et %s (Appuyez sur une touche pour commencer)",$playerOne->getName(),$playerTwo->getName()."\n"));
$game->init([$playerOne,$playerTwo]);

$inc =0;
while($game->trade()){	

	++$inc;
	//Handle displays
	View::displayTrade($game,$inc);
}

//4 - End Game 
View::displayWinner($game);